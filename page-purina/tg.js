"use strict";
var $ = require("jquery");
var header = require("./src/header/index.js");
var recettes_content = require("./src/content/index.js");
var settings = require("./setting.json");
var sto = window.__sto;
var current;

function post(method, value) {
    var result = {
        "method": method
    };
    if (value !== undefined) {
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");
}

function trackerMethod(trackingObj) {
    var result = {
        "method": "tracking",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");

}
function trackerBrowse(trackingObj) {
    var result = {
        "method": "trackingBrowse",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerClick(trackingObj) {
    var result = {
        "method": "trackingClick",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerMoreInfo(trackingObj) {
    var result = {
        "method": "trackingMoreInfo",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyMore(trackingObj) {
    var result = {
        "method": "trackingQtyMore",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyLess(trackingObj) {
    var result = {
        "method": "trackingQtyLess",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyChange(trackingObj) {
    var result = {
        "method": "trackingQtyChange",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerAddToList(trackingObj) {
    var result = {
        "method": "trackingAddToList",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}



function trackerBasket(basket, trackingObj) {
    var result = {
        "method": "trackbasket",
        "basket": JSON.stringify(basket),
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");

}


$('.sto-recettes-list-item').on('click', function() {
    post("selectcrawl", $(this).attr('data-name'))
    current = $(this).attr('data-name');
    //post("crawl", results);
});


window.addEventListener("message", function(e) {
    var data;
    try {
        data = JSON.parse(e.data);
    } catch (e) {
        return;
    }

    switch (data.method) {

        case "crawl":
            jajoute(data.value);
            break;
        case "crawlcereales":
            cerealesProds(data.value);
            break;
        case "crawlpurina":
            purinaProds(data.value);
            break;

    }
});

post("crawlPurina");

var purinaProds = function(lecrawl) {

    var productos = settings['purina']['produits'];


    var len = $.map(lecrawl, function(n, i) {
        return i;
    }).length;

    $.each(productos, function(i, v) {

        var porductosporanimal = 0;


        var purinaAnimal = $('<div class="sto-purina-porducts-' + i + '"></div>');

        $.each(v, function(ind, val) {

            porductosporanimal++;

            var purinaSection = $('<div class="sto-purina-section-' + ind + '" data-sec="' + porductosporanimal + '"><div class="sto-purina-noprod">Produit temporairement indisponible.</div></div>');


            for (var a = 0; a < val.length; a++) {
                var index = val[a];

                if (lecrawl[index]) {
                    var value = lecrawl[index];

                    var purinaFullProd = value['fullprod'];

                    purinaSection.append(purinaFullProd);

                }

            }

            purinaAnimal.append(purinaSection);
        });

        $('.sto-purina-porducts-container-' + i).append(purinaAnimal);
    });

    var imagenes = $('#sto-mbr-tg-purina .item .picture img.photo');
    for (var b = 0; b < imagenes.length; b++) {
        var srcImg = $(imagenes[b]).attr('src').substring(1);
        $(imagenes[b]).attr('src', '//www.coradrive.fr' + srcImg)
    }
    var imagenes2 = $('#sto-mbr-tg-purina .item .picture img:not(".photo"), #sto-mbr-tg-purina .avantages img, #sto-mbr-tg-purina img.ribbon');
    for (var b = 0; b < imagenes2.length; b++) {
        var srcImg = $(imagenes2[b]).attr('src');
        $(imagenes2[b]).attr('src', '//www.coradrive.fr/' + srcImg)
    }

    $('#sto-mbr-tg-purina .avantages img').remove();
    $('.sto-purina-section-chat_adulte').attr('id', 'sto-chat-section-current');
    $('.sto-purina-section-chien_adulte').attr('id', 'sto-chien-section-current');



    // <editor-fold> INIT CAROUSEL ********************************************************
    var initChat = $('.sto-purina-porducts-chat div#sto-chat-section-current');
    var initCarouselIndicatorChat = $('.sto-purina-frame2-nav-indicators-item');

    if (initChat.find('.item').length < 5 && initCarouselIndicatorChat.eq(1).css('display') == 'block') {
      initCarouselIndicatorChat.eq(1).css('display', 'none');
      $('.sto-purina-frame2-arrow-left, .sto-purina-frame2-arrow-right').hide();
    } else if (initChat.find('.item').length >= 5 && initCarouselIndicatorChat.eq(1).css('display') == 'none') {
      initCarouselIndicatorChat.eq(1).css('display', 'block');
      $('.sto-purina-frame2-arrow-left, .sto-purina-frame2-arrow-right').show();
    }
    $('.sto-purina-porducts-chat div').css('left', '0px');
    $('.sto-purina-frame2-nav-indicators-item').removeClass('sto-purina-frame2-nav-indicators-current');
    $('.sto-purina-frame2-nav-indicators-item:nth-of-type(1)').eq(0).addClass('sto-purina-frame2-nav-indicators-current');


    var initChien = $('.sto-purina-porducts-chien div#sto-chien-section-current');
    var initCarouselIndicatorChien = $('.sto-purina-frame3-nav-indicators-item');

    if (initChien.find('.item').length < 5 && initCarouselIndicatorChien.eq(1).css('display') == 'block') {
      initCarouselIndicatorChien.eq(1).css('display', 'none');
      $('.sto-purina-frame3-arrow-left, .sto-purina-frame3-arrow-right').hide();
    } else if (initChien.find('.item').length >= 5 && initCarouselIndicatorChien.eq(1).css('display') == 'none') {
      initCarouselIndicatorChien.eq(1).css('display', 'block');
      $('.sto-purina-frame3-arrow-left, .sto-purina-frame3-arrow-right').show();
    }
    $('.sto-purina-porducts-chien div').css('left', '0px');
    $('.sto-purina-frame3-nav-indicators-item').removeClass('sto-purina-frame3-nav-indicators-current');
    $('.sto-purina-frame3-nav-indicators-item:nth-of-type(1)').eq(0).addClass('sto-purina-frame3-nav-indicators-current');


    // </editor-fold> *********************************************************************




    $('.sto-purina-frame2-nav-item').on('click', function() {
        $('.sto-purina-frame2-nav-item').attr('class', 'sto-purina-frame2-nav-item');
        var arrowNav = $('.nav-chats-arrow').detach();
        $(this).addClass('purina-nav-current').append(arrowNav);
        var selectSection = $(this).attr('data-sec');
        $('.sto-purina-porducts-chat>div').removeAttr("id");
        $('.sto-purina-porducts-chat>div[data-sec="' + selectSection + '"]').attr('id', 'sto-chat-section-current');

        var currentChat = $('.sto-purina-porducts-chat div#sto-chat-section-current');
        var carouselIndicator = $('.sto-purina-frame2-nav-indicators-item');

        if (currentChat.find('.item').length < 5 && carouselIndicator.eq(1).css('display') == 'block') {
          carouselIndicator.eq(1).css('display', 'none');
          $('.sto-purina-frame2-arrow-left, .sto-purina-frame2-arrow-right').hide();
        } else if (currentChat.find('.item').length >= 5 && carouselIndicator.eq(1).css('display') == 'none') {
          carouselIndicator.eq(1).css('display', 'block');
          $('.sto-purina-frame2-arrow-left, .sto-purina-frame2-arrow-right').show();
        }
        $('.sto-purina-porducts-chat div').css('left', '0px');
        $('.sto-purina-frame2-nav-indicators-item').removeClass('sto-purina-frame2-nav-indicators-current');
        $('.sto-purina-frame2-nav-indicators-item:nth-of-type(1)').eq(0).addClass('sto-purina-frame2-nav-indicators-current');
    });

    $('.sto-purina-frame3-nav-item').on('click', function() {
        $('.sto-purina-frame3-nav-item').attr('class', 'sto-purina-frame3-nav-item');
        var arrowNav = $('.nav-chiens-arrow').detach();
        $(this).addClass('purina-nav-current').append(arrowNav);
        var selectSection = $(this).attr('data-sec');
        $('.sto-purina-porducts-chien>div').removeAttr("id");
        $('.sto-purina-porducts-chien>div[data-sec="' + selectSection + '"]').attr('id', 'sto-chien-section-current');

        var currentChien = $('.sto-purina-porducts-chien div#sto-chien-section-current');
        var carouselIndicator = $('.sto-purina-frame3-nav-indicators-item');

        if (currentChien.find('.item').length < 5 && carouselIndicator.eq(1).css('display') == 'block') {
          carouselIndicator.eq(1).css('display', 'none');
          $('.sto-purina-frame3-arrow-left, .sto-purina-frame3-arrow-right').hide();
        } else if (currentChien.find('.item').length >= 5 && carouselIndicator.eq(1).css('display') == 'none') {
          carouselIndicator.eq(1).css('display', 'block');
          $('.sto-purina-frame3-arrow-left, .sto-purina-frame3-arrow-right').show();
        }
        $('.sto-purina-porducts-chien div').css('left', '0px');
        $('.sto-purina-frame3-nav-indicators-item').removeClass('sto-purina-frame3-nav-indicators-current');
        $('.sto-purina-frame3-nav-indicators-item:nth-of-type(1)').eq(0).addClass('sto-purina-frame3-nav-indicators-current');
    });



    // <editor-fold> WIDTH CAROUSEL *******************************************************
    $('.sto-purina-porducts-chat').children().each(function() {
      var nbChat = $(this).children('.item').length;
      var widthChat = nbChat * 224;
      $(this).css('width', widthChat + 'px');
    });

    $('.sto-purina-porducts-chien').children().each(function() {
      var nbChien = $(this).children('.item').length;
      var widthChien = nbChien * 224;
      $(this).css('width', widthChien + 'px');
    });
    // </editor-fold> *********************************************************************


    // <editor-fold> CAROUSEL *************************************************************
    $('.sto-purina-frame2-arrow-right, .sto-purina-frame2-nav-indicators-item:nth-of-type(2)').on('click', function() {
        var currentChat = $('.sto-purina-porducts-chat div#sto-chat-section-current');
        if ($('.sto-purina-frame2-nav-indicators-item').eq(1).css('display') == 'block') {
          currentChat.animate({
              left: "-896",
          }, 500);
          $('.sto-purina-frame2-nav-indicators-item').attr('class', 'sto-purina-frame2-nav-indicators-item');
          $('.sto-purina-frame2-nav-indicators-item:nth-child(2)').addClass('sto-purina-frame2-nav-indicators-current');
        }
      });
    $('.sto-purina-frame2-arrow-left, .sto-purina-frame2-nav-indicators-item:nth-child(1)').on('click', function() {
        var currentChat = $('.sto-purina-porducts-chat div#sto-chat-section-current');
        var nbChat = $('.sto-purina-porducts-chat div#sto-chat-section-current').children('.item').length;
        currentChat.animate({
            left: "0",
        }, 500);
        $('.sto-purina-frame2-nav-indicators-item').attr('class', 'sto-purina-frame2-nav-indicators-item');
        $('.sto-purina-frame2-nav-indicators-item:nth-child(1)').addClass('sto-purina-frame2-nav-indicators-current');
      });


      $('.sto-purina-frame3-arrow-right, .sto-purina-frame3-nav-indicators-item:nth-of-type(2)').on('click', function() {
          var currentChien = $('.sto-purina-porducts-chien div#sto-chien-section-current');
          if ($('.sto-purina-frame3-nav-indicators-item').eq(1).css('display') == 'block') {
            currentChien.animate({
                left: "-896",
            }, 500);
            $('.sto-purina-frame3-nav-indicators-item').attr('class', 'sto-purina-frame3-nav-indicators-item');
            $('.sto-purina-frame3-nav-indicators-item:nth-child(2)').addClass('sto-purina-frame3-nav-indicators-current');
          }
        });
      $('.sto-purina-frame3-arrow-left, .sto-purina-frame3-nav-indicators-item:nth-child(1)').on('click', function() {
          var currentChien = $('.sto-purina-porducts-chien div#sto-chien-section-current');
          var nbChien = $('.sto-purina-porducts-chien div#sto-chien-section-current').children('.item').length;
          currentChien.animate({
              left: "0",
          }, 500);
          $('.sto-purina-frame3-nav-indicators-item').attr('class', 'sto-purina-frame3-nav-indicators-item');
          $('.sto-purina-frame3-nav-indicators-item:nth-child(1)').addClass('sto-purina-frame3-nav-indicators-current');
        });
    // </editor-fold> *********************************************************************


    $('#sto-mbr-tg-purina .item .actions .ajout-panier').on('click', function() {

        var prodBuyPurina = $(this).closest('.actions').addClass('sto-prod-buy').wrap('<div>').parent().html();
        post("addtocart", prodBuyPurina);
    });

    $('#sto-mbr-tg-purina .item .actions .liste').on('click', function() {
        var prodSavePurina = $(this).closest('.actions').addClass('sto-prod-save').wrap('<div>').parent().html();
        post("addtolist", prodSavePurina);
    });

    $('#sto-mbr-tg-purina .item .actions .plus').on('click', function() {
        var quantity = parseInt($(this).closest('.actions').find('input[name="qty"]').val());
        $(this).closest('.actions').find('input[name="qty"]').attr('value', quantity + 1);
    });
    $('#sto-mbr-tg-purina .item .actions .moins').on('click', function() {
        var quantity = parseInt($(this).closest('.actions').find('input[name="qty"]').val());
        if (quantity > 1) {
            $(this).closest('.actions').find('input[name="qty"]').attr('value', quantity - 1);
        }
    });
    $('input[name="qty"]').on('keyup', function() {
        var quantity = $(this).val();
        if (quantity < 1 || !quantity) {
            quantity = 1;
            $(this).val(quantity);
        }
        $(this).attr('value', quantity);
        $(this).blur();
    });



    $('.sto-purina-frame1-decou-chiens, .sto-cereales-frame1-decou-barres').on('click', function(e) {
        post("scrolldog");
    });
    $('.sto-purina-frame1-decou-chats, .sto-cereales-frame1-decou-paquets').on('click', function(e) {
        post("scrollcat");
    });
    $('.sto-cereales-frame1-savoir').on('click', function(e) {
        post("scrollensavoir");
    });

    var purinasectionchat = $('.sto-purina-porducts-chat>div');
    var purinasectionchien = $('.sto-purina-porducts-chien>div');
    for (var c = 0; c < purinasectionchat.length; c++) {
        if ($(purinasectionchat[c]).find('.item').length > 0) {
            $(purinasectionchat[c]).find('.sto-purina-noprod').hide();
        }
    }
    for (var c = 0; c < purinasectionchien.length; c++) {
        if ($(purinasectionchien[c]).find('.item').length > 0) {
            $(purinasectionchien[c]).find('.sto-purina-noprod').hide();
        }
    }

    $('#sto-mbr-tg-purina .actions .liste').on("click", function() {
        var producto = $(this).attr('data-prd');
        trackerAddToList({
            "tl": producto,
        });
    });
    $('#sto-mbr-tg-purina .actions .moins').on("click", function() {
          var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
          var name = $(this).closest('.item').find('div.desc p a').text();
          var quantity = $(this).closest('.item').find('input[name="qty"]').val();
          var price = $(this).closest('.item').find('.prix-produit').text();
          var promo;
          if ($(this).closest('.item').find('.avantages').length > 0) {
              promo = true;
          } else {
              promo = false;
          }
          trackerQtyLess([{
            "id": product,
            "qty": quantity,
            "promo": promo,
            "name": name,
            "price": price
        }]);
    });
    $('#sto-mbr-tg-purina .actions .plus').on("click", function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyMore([{
          "id": product,
          "qty": quantity,
          "promo": promo,
          "name": name,
          "price": price
      }]);
    });
    $('#sto-mbr-tg-purina .actions input').on('change', function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyChange([{
          "id": product,
          "qty": quantity,
          "promo": promo,
          "name": name,
          "price": price
      }]);
    });
    $('#sto-mbr-tg-purina .actions .ajout-panier').on('click', function() {

        var product = $(this).attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages')) {
            promo = true;
        } else {
            promo = false;
        }
        trackerBasket([{
            "id": product,
            "name": name,
            "price": price,
            "qty": quantity,
            "promo": promo
        }], {
            "ta": "abk",
            "te": "abk-btn"
        });

    });
    $('#sto-mbr-tg-purina .picture a, #sto-mbr-tg-purina .desc p a').on('click', function(e){
      e.preventDefault();
      var product = $(this).closest('.item').find('.ajout-panier').attr('data-prd');
      trackerMoreInfo({
          "tl": product,
      });
      var productLink = $(this).attr('href');
      post("seeprod", productLink);
    });


};




$('.sto-mbr-tg-jecontinue').on('click', function(e) {
    e.preventDefault();
    post("outofmbr");
});
$('.sto-mbr-tg-logo, .nav-recettes').on('click', function(e) {
    e.preventDefault();
    post("gotohome", "home");
});

$('.sto-mbr-nav-item.nav-equilibre').on('click', function(e) {
    e.preventDefault();
    post("gotohome", "equilibre");
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function(e) {
    e.preventDefault();
    post("gotocereales");
});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function(e) {

});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function(e) {
    e.preventDefault();
    post("gotowater");
});



//$(".sto-recette-icons").mCustomScrollbar();


/**     TRACKING     **/

/*****  header  *****/
$('.sto-mbr-tg-jecontinue').on('click', function() {
    trackerClick({
        "tl": "Close"
    });
});
$('.sto-mbr-tg-logo').on('click', function() {
    trackerClick({
        "tl": "HomeLogo"
    });
});
$('.sto-mbr-nav-item.nav-recettes').on('click', function() {
    trackerClick({
        "tl": "MainNavHome"
    });
});
$('.sto-mbr-nav-item.nav-equilibre').on('click', function() {
    trackerClick({
        "tl": "MainNavEquilibre"
    });
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function() {
    trackerClick({
        "tl": "MainNavCereales"
    });
});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function() {
    trackerClick({
        "tl": "MainNavPurina"
    });
});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function(e) {
    trackerClick({
        "tl": "MainNavHydratation"
    });
});




/*****  Purina  *****/
$('.sto-purina-frame1-decou-chiens').on("click", function() {
    trackerClick({
        "tl": "DecouvirChiens"
    });
});
$('.sto-purina-frame1-decou-chats').on("click", function() {
    trackerClick({
        "tl": "DecouvirChat"
    });
});
$('.sto-purina-frame2-nav-item').on("click", function() {
    var trackName = $(this).attr('data-sec');
    trackerBrowse({
        "tl": "NavChat" + trackName
    });
});
$('.sto-purina-frame3-nav-item').on("click", function() {
    var trackName = $(this).attr('data-sec');
    trackerBrowse({
        "tl": "NavChiens" + trackName
    });
});

$('.sto-purina-frame2-arrow-left').on("click", function() {
    trackerBrowse({
        "tl": "NavCarouselChatLeft"
    });
});
$('.sto-purina-frame2-arrow-right').on("click", function() {
    trackerBrowse({
        "tl": "NavCarouselChatRight"
    });
});
$('.sto-purina-frame2-nav-indicators').on("click", function() {
    trackerBrowse({
        "tl": "NavCarouselChat"
    });
});

$('.sto-purina-frame3-arrow-left').on("click", function() {
    trackerBrowse({
        "tl": "NavCarouselChienLeft"
    });
});
$('.sto-purina-frame3-arrow-right').on("click", function() {
    trackerBrowse({
        "tl": "NavCarouselChienRight"
    });
});
$('.sto-purina-frame3-nav-indicators').on("click", function() {
    trackerBrowse({
        "tl": "NavCarouselChien"
    });
});
