"use strict";
__sto.load("TG", function(tracker) {
    tracker.display();
    const document = global.document;
    const $ = global.jQuery;
    // const baseUrl = "//localhost:8080";
    const baseUrl = "//rscdn.storetail.net/coradrive/nestle/mesbonsreflexesV3/page-purina/";
    const iframe = document.createElement("IFRAME");
    var settings = require("./setting.json");
    var resultsPurina = {};

    $('body').css('background-image','none');

    iframe.src = baseUrl;
    iframe.id = 'mbrapp';

    $('.wrapinner h1').text('Nos amis les bêtes');

    var drive = document.location.toString().split('/')[3];

    function post(method, value) {
        var result = {
            "method": method
        };
        if (value !== undefined) {
            result.value = value;
        }
        var win = document.getElementById("mbrapp").contentWindow;
        win.postMessage(JSON.stringify(result), "*");

    }

    function trackerEvent(data) {
        tracker.sendHit(JSON.parse(data.value));
    }

    function trackbasket(data) {
        tracker.basket(JSON.parse(data.basket), JSON.parse(data.value));
    }

    function trackerMethod(trackingObj) {
        var result = {
            "method": "tracking",
            "value": JSON.stringify(trackingObj)
        };
        window.postMessage(JSON.stringify(result), "*");
    }

    function trackerBrowse(data) {
      tracker.browse(JSON.parse(data.value));
    }
    function trackerClick(data) {
      tracker.click(JSON.parse(data.value));
    }
    function trackerMoreInfo(data) {
      tracker.moreInfo(JSON.parse(data.value));
    }
    function trackerQtyMore(data) {
      tracker.quantityMore(JSON.parse(data.value));
    }
    function trackerQtyLess(data) {
      tracker.quantityLess(JSON.parse(data.value));
    }
    function trackerQtyChange(data) {
      tracker.quantityChange(JSON.parse(data.value));
    }
    function trackerAddToList(data) {
      tracker.addToList(JSON.parse(data.value));
    }


    window.addEventListener("message", function(e) {
        var data;
        try {
            data = JSON.parse(e.data);
        } catch (e) {
            return;
        }
        switch (data.method) {

            case "trackingBrowse":
                trackerBrowse(data);
                break;
            case "trackingClick":
                trackerClick(data);
                break;
            case "trackingMoreInfo":
                trackerMoreInfo(data);
                break;
            case "trackingQtyMore":
                trackerQtyMore(data);
                break;
            case "trackingQtyLess":
                trackerQtyLess(data);
                break;
            case "trackingQtyChange":
                trackerQtyChange(data);
                break;
            case "trackingAddToList":
                trackerAddToList(data);
                break;
            case "trackbasket":
                trackbasket(data);
                break;
            case "selectcrawl":
                crawler(data.value);
                //post("crawl", results);
                break;
            case "addtocart":
                addtocart(data.value);
                break;
            case "addtolist":
                addtolist(data.value);
                break;
            case "scrolltop":
                scrolltop();
                break;
            case "scrolldog":
                scrolldog();
                break;
            case "scrollcat":
                scrollcat();
                break;
            case "scrollensavoir":
                scrollensavoir();
                break;
            case "bigtg":
                bigtg();
                break;
            case "smalltg":
                smalltg();
                break;
            case "smallertg":
                smallertg();
                break;
            case "outofmbr":
                outofmbr();
                break;
            case "gotocereales":
                gotocereales();
                break;
            case "gotowater":
                gotowater();
                break;
            case "gotohome":
                gotohome(data.value);
                break;
            case "seeprod":
                seeprod(data.value);
                break;
            case "crawlPurina":
                crawlerPurina();
                break;
        }
    });



    function crawlerPurina() {

        var crawlLinkOriginal = settings['purina']['crawl_purina'].toString().split('/');
        crawlLinkOriginal[3] = drive;
        var crawlLinkNew = '';

        for (var d = 0; d < crawlLinkOriginal.length; d++) {
            crawlLinkNew = crawlLinkNew + crawlLinkOriginal[d] + '/';
        }

        $.get("/" + drive + "/tous-les-rayons/promo-et-offres/annonces.html?u=786", function(data1)  {

          $.get(crawlLinkNew, function(data2) {
              var data = $.merge($(data1), $(data2));

              $.when(data.find(".item").each(function(i, v) {

                if (!$(this).hasClass('soon_available')) {
                  var prod = {
                      "id": $(v).find(".idProduit").attr("value"),
                      "libelle": $(v).find(".desc p a").text(),
                      "img": $(v).find(".picture a img").attr('src').substring(1),
                      "prix": $(v).find(".price .prix-produit").html(),
                      "prixsmall": $(v).find(".price .info").html().split('<br>'),
                      "addtocart": $(v).find(".actions").wrap('<div>').parent().html(),
                      "fullprod": $(this).wrap('<div>').parent().html()
                  };

                  resultsPurina[prod.id] = prod;
                }



              })).done(post("crawlpurina", resultsPurina));

          });
        });


        //setTimeout(function() {
        //    post("crawlpurina", resultsPurina);
        //}, 3000);
    }
    // crawlerPurina();

    function seeprod(data){
      window.open( data , "_self");
    }


    function scrolltop() {
        $('html, body').animate({
            scrollTop: 0
        }, 0);
    }

    function scrolldog() {
        $('html, body').animate({
            scrollTop: 1025
        }, 1000);
    }

    function scrollcat() {
        $('html, body').animate({
            scrollTop: 456
        }, 500);
    }

    function scrollensavoir() {
        $('html, body').animate({
            scrollTop: 1603
        }, 1000);
    }

    function bigtg() {
        $('body').find('#main').css("height", "2150px");
        $('body').find('#main>div').css("height", "2181px");
    }

    function smalltg() {
        $('body').find('#main').css("height", "830px");
        $('body').find('#main>div').css("height", "861px");
    }

    function smallertg() {
        $('body').find('#main').css("height", "1700px");
        $('body').find('#main>div').css("height", "1731px");
    }

    function outofmbr() {
        window.open("//www.coradrive.fr/"+drive+"/", "_self");
    }

    function gotocereales() {
        window.open("//www.coradrive.fr/"+drive+"/pages/evenement2.html", "_self");
    }

    function gotowater() {
        window.open("//www.coradrive.fr/"+drive+"/pages/evenement4.html", "_self");
    }

    function gotohome(data) {
        if (data == "equilibre") {
            window.open("//www.coradrive.fr/"+drive+"/pages/evenement1.html?equilibre", "_self");
        } else {
            window.open("//www.coradrive.fr/"+drive+"/pages/evenement1.html", "_self");
        }

    }

    function addtocart(data) {
        $('body').append(data);
        $('.sto-prod-buy').hide();
        $('.sto-prod-buy .ajout-panier').last().attr({"data-src":"srl","data-op":"tgdigstoretailsem432016"}).addClass('ajout-panier-externe').removeClass('ajout-panier');
        $('.sto-prod-buy .ajout-panier-externe').last().click();
    }

    function addtolist(data) {
        $('body').append(data);
        $('.sto-prod-save').hide();
        $('.sto-prod-save .liste').last().click();
    }

    $('html').removeClass("headerpasfixe");
    $('#lockheader').hide();



    $(function() {

        const $content = $("#main").empty();
        $(".wrapinnertop").css("position","relative");
        $("body").css("background-image", "none");


        $content.css("height", "1681px");
        const $container = $("<div>")
            .css({
                height: "1681px",
                position: "absolute",
                left: "0",
                right: "0",
                marginTop: "-1px"
            })
            .append(
                $(iframe)
                .css({
                    position: "relative",
                    width: "100%",
                    height: "100%"
                })
            )
            .appendTo("#main");
    });

    $('html, body').animate({
        scrollTop: 0
    }, 0);

});
