"use strict";
var $ = require("jquery");
var header = require("./src/header/index.js");
var recettes_content = require("./src/content/index.js");
var settings = require("./setting.json");
var sto = window.__sto;
var current;


function post(method, value) {
    var result = {
        "method": method
    };
    if (value !== undefined) {
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");
}

// function trackerMethod(trackingObj) {
//     var result = {
//         "method": "tracking",
//         "value": JSON.stringify(trackingObj)
//     };
//     window.parent.postMessage(JSON.stringify(result), "*");
// }

function trackerBrowse(trackingObj) {
    var result = {
        "method": "trackingBrowse",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}

function trackerClick(trackingObj) {
    var result = {
        "method": "trackingClick",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerMoreInfo(trackingObj) {
    var result = {
        "method": "trackingMoreInfo",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyMore(trackingObj) {
    var result = {
        "method": "trackingQtyMore",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyLess(trackingObj) {
    var result = {
        "method": "trackingQtyLess",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyChange(trackingObj) {
    var result = {
        "method": "trackingQtyChange",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerAddToList(trackingObj) {
    var result = {
        "method": "trackingAddToList",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}

function trackerBasket(basket, trackingObj) {
    var result = {
        "method": "trackbasket",
        "basket": JSON.stringify(basket),
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}


window.addEventListener("message", function(e) {
    var data;
    try {
        data = JSON.parse(e.data);
    } catch (e) {
        return;
    }

    switch (data.method) {

        case "crawl":
            jajoute(data.value);
            break;
        case "crawlcereales":
            cerealesProds(data.value);
            break;
        case "crawlpurina":
            purinaProds(data.value);
            break;

    }
});

post("crawlWater");



var cerealesProds = function(lecrawl) {

    var productos = settings['cereales']['produits'];


    var len = $.map(lecrawl, function(n, i) {
        return i;
    }).length;

    $.each(productos, function(i, val) {

        var numProds = 0;
        var pordContainer = $('<div class="sto-cereales-products-' + i + '"></div>');


        for (var a = 0; a < val.length; a++) {
            var index = val[a];

            if (lecrawl[index]) {
                var value = lecrawl[index];

                var cerealesFullProd = value['fullprod'];
                // console.log(cerealesFullProd);
                pordContainer.append(cerealesFullProd);
                numProds++;
            }


        }

        //$('.sto-cereales-products-'+ i).attr('data-prod','3');
        $('.sto-cereales-' + i + '-carrousel').append(pordContainer);
        $('.sto-cereales-products-' + i).attr('data-prod', numProds);

        $('.sto-purina-frame1-decou-chiens, .sto-cereales-frame1-decou-barres').on('click', function(e) {
            post("scrolldog");
        });
        $('.sto-purina-frame1-decou-chats, .sto-cereales-frame1-decou-paquets').on('click', function(e) {
            post("scrollcat");
        });
        $('.sto-cereales-frame1-savoir').on('click', function(e) {
            post("scrollensavoir");
        });


        //console.log(val.length);

    });

    var widthPaquets = $('.sto-cereales-products-paquets').attr('data-prod') * 224;
    $('.sto-cereales-products-paquets').css('width', widthPaquets + 'px');

    var widthBarres = $('.sto-cereales-products-barres').attr('data-prod') * 224;
    $('.sto-cereales-products-barres').css('width', widthBarres + 'px');

    var imagenes = $('#sto-mbr-tg-cereales .item .picture img.photo, #sto-mbr-tg-cereales .item .picture img:not(".photo"), #sto-mbr-tg-global .avantages p img, #sto-mbr-tg-cereales img.ribbon');
    // console.log(imagenes);
    for (var b = 0; b < imagenes.length; b++) {
        var srcImg = $(imagenes[b]).attr('src');
        if(srcImg[0] != "/"){
            $(imagenes[b]).attr('src', '//www.coradrive.fr/' + srcImg);
        }else{
            $(imagenes[b]).attr('src', '//www.coradrive.fr' + srcImg);
        }

    }

    $('#sto-mbr-tg-global .avantages p img').remove();

    $('.sto-cereales-frame2-left, .sto-cereales-frame2-nav-item:nth-child(1)').on('click', function() {
        $('.sto-cereales-products-paquets').animate({
            left: "0",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-cereales-frame2-nav-item').attr('class', 'sto-cereales-frame2-nav-item');
        $('.sto-cereales-frame2-nav-item:nth-child(1)').addClass('sto-cereales-frame2-nav-current');
    });
    $('.sto-cereales-frame2-right, .sto-cereales-frame2-nav-item:nth-child(2)').on('click', function() {
        $('.sto-cereales-products-paquets').animate({
            left: "-896",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-cereales-frame2-nav-item').attr('class', 'sto-cereales-frame2-nav-item');
        $('.sto-cereales-frame2-nav-item:nth-child(2)').addClass('sto-cereales-frame2-nav-current');
    });

    $('.sto-cereales-frame3-left, .sto-cereales-frame3-nav-item:nth-child(1)').on('click', function() {
        $('.sto-cereales-products-barres').animate({
            left: "0",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-cereales-frame3-nav-item').attr('class', 'sto-cereales-frame3-nav-item');
        $('.sto-cereales-frame3-nav-item:nth-child(1)').addClass('sto-cereales-frame3-nav-current');
    });
    $('.sto-cereales-frame3-right, .sto-cereales-frame3-nav-item:nth-child(2)').on('click', function() {
        $('.sto-cereales-products-barres').animate({
            left: "-896",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-cereales-frame3-nav-item').attr('class', 'sto-cereales-frame3-nav-item');
        $('.sto-cereales-frame3-nav-item:nth-child(2)').addClass('sto-cereales-frame3-nav-current');
    });


    $('#sto-mbr-tg-cereales .item .actions .ajout-panier').on('click', function() {
        var product = $(this).attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerBasket([{
            "id": product,
            "name": name,
            "price": price,
            "qty": quantity,
            "promo": promo
        }], {
            "ta": "abk",
            "te": "abk-btn"
        });
        var prodBuyCereales = $(this).closest('.actions').addClass('sto-prod-buy').wrap('<div>').parent().html();
        post("addtocart", prodBuyCereales);
    });

    $('#sto-mbr-tg-cereales .item .actions .liste').on('click', function() {

        var prodSaveCereales = $(this).closest('.actions').addClass('sto-prod-save').wrap('<div>').parent().html();
        post("addtolist", prodSaveCereales);
    });

    $('#sto-mbr-tg-cereales .item .actions .plus').on('click', function() {
        var quantity = parseInt($(this).closest('.actions').find('input[name="qty"]').val());
        $(this).closest('.actions').find('input[name="qty"]').attr('value', quantity + 1);
    });
    $('#sto-mbr-tg-cereales .item .actions .moins').on('click', function() {
        var quantity = parseInt($(this).closest('.actions').find('input[name="qty"]').val());
        if (quantity > 1) {
            $(this).closest('.actions').find('input[name="qty"]').attr('value', quantity - 1);
        }
    });
    $('input[name="qty"]').on('keyup', function() {
        var quantity = $(this).val();
        if (quantity < 1 || !quantity) {
            quantity = 1;
            $(this).val(quantity);
        }
        $(this).attr('value', quantity);
        $(this).blur();
    });

    var qteProdsPaquets = $('.sto-cereales-products-paquets .item').length;
    var qteProdsBarres = $('.sto-cereales-products-barres .item').length;
    if (qteProdsPaquets < 5) {
        $('.sto-cereales-frame2-left, .sto-cereales-frame2-right, .sto-cereales-frame2-nav').hide();
    }
    if (qteProdsPaquets > 0) {
        $('.sto-cereales-frame2-noprod').hide();
    }
    if (qteProdsBarres < 5) {
        $('.sto-cereales-frame3-left, .sto-cereales-frame3-right, .sto-cereales-frame3-nav').hide();
    }
    if (qteProdsBarres > 0) {
        $('.sto-cereales-frame3-noprod').hide();
    }

    $('#sto-mbr-tg-cereales .actions .liste').on("click", function() {
        var producto = $(this).attr('data-prd');
        trackerAddToList({
            "tl": producto,
        });
    });
    $('#sto-mbr-tg-cereales .actions .moins').on("click", function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyLess([{
        "id": product,
        "qty": quantity,
        "promo": promo,
        "name": name,
        "price": price
      }]);
    });
    $('#sto-mbr-tg-cereales .actions .plus').on("click", function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyMore([{
        "id": product,
        "qty": quantity,
        "promo": promo,
        "name": name,
        "price": price
      }]);
    });
    $('#sto-mbr-tg-cereales .actions input').on('change', function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyChange([{
        "id": product,
        "qty": quantity,
        "promo": promo,
        "name": name,
        "price": price
      }]);
    });
    $('#sto-mbr-tg-cereales .picture a, #sto-mbr-tg-purina .desc p a').on('click', function(e){
      e.preventDefault();
      var product = $(this).closest('.item').find('.ajout-panier').attr('data-prd');
      trackerMoreInfo({
          "tl": product,
      });
      var productLink = $(this).attr('href');
      post("seeprod", productLink);
    });


};



$('.sto-mbr-tg-jecontinue').on('click', function(e) {
    e.preventDefault();
    post("outofmbr");
});
$('.sto-mbr-tg-logo, .nav-recettes').on('click', function(e) {
    e.preventDefault();
    post("gotohome", "home");
});

$('.sto-mbr-nav-item.nav-equilibre').on('click', function(e) {
    e.preventDefault();
    post("gotohome", "equilibre");
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function(e) {

});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function(e) {
    e.preventDefault();
    post("gotopurina");
});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function(e) {
    e.preventDefault();
    post("gotowater");
});

setInterval(function(){
    if($(document).find('.avantages').length != 0){
        $(document).find('.avantages').on('click', function(e) {
            if($(this).find("ul").hasClass("open")){
                $(this).find("ul").removeClass("open");
            }else{
                $(this).find("ul").addClass("open");
            }
        });
    }
},20);

//$(".sto-recette-icons").mCustomScrollbar();


/****************************      TRACKIIIIIIIIIIING      *************************************/

/*****  header  *****/
$('.sto-mbr-tg-jecontinue').on('click', function() {
    trackerClick({
        "tl": "Close"
    });
});
$('.sto-mbr-tg-logo').on('click', function() {
    trackerClick({
        "tl": "HomeLogo"
    });
});
$('.sto-mbr-nav-item.nav-recettes').on('click', function() {
    trackerClick({
        "tl": "MainNavHome"
    });
});
$('.sto-mbr-nav-item.nav-equilibre').on('click', function() {
    trackerClick({
        "tl": "MainNavEquilibre"
    });
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function() {
    trackerClick({
        "tl": "MainNavCereales"
    });
});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function() {
    trackerClick({
        "tl": "MainNavPurina"
    });
});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function() {
    trackerClick({
        "tl": "MainNavHydratation"
    });
});

/*****  Cereales  *****/
$('.sto-cereales-frame1-decou-paquets').on("click", function() {
    trackerClick({
        "tl": "DecouvirPaquets"
    });
});
$('.sto-cereales-frame1-decou-barres').on("click", function() {
    trackerClick({
        "tl": "DecouvirBarres"
    });
});
$('.sto-cereales-frame1-savoir').on("click", function() {
    trackerClick({
        "tl": "CerealesEnSavoirPlus"
    });
});
$('.sto-cereales-frame2-left').on("click", function() {
    trackerBrowse({
        "tl": "NavPaquetsLeft"
    });
});
$('.sto-cereales-frame2-right').on("click", function() {
    trackerBrowse({
        "tl": "NavPaquetsLeft"
    });
});
$('.sto-cereales-frame2-nav').on("click", function() {
    trackerBrowse({
        "tl": "NavPaquets"
    });
});
$('.sto-cereales-frame3-left').on("click", function() {
    trackerBrowse({
        "tl": "NavBarresLeft"
    });
});
$('.sto-cereales-frame3-right').on("click", function() {
    trackerBrowse({
        "tl": "NavBarresLeft"
    });
});
$('.sto-cereales-frame3-nav').on("click", function() {
    trackerBrowse({
        "tl": "NavBarres"
    });
});
