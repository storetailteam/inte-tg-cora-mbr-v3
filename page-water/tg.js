"use strict";
var $ = require("jquery");
var header = require("./src/header/index.js");
var recettes_content = require("./src/content/index.js");
var settings = require("./setting.json");
var sto = window.__sto;
var current;


function post(method, value) {
    var result = {
        "method": method
    };
    if (value !== undefined) {
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");
}

function trackerMethod(trackingObj) {
    var result = {
        "method": "tracking",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerBrowse(trackingObj) {
    var result = {
        "method": "trackingBrowse",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerClick(trackingObj) {
    var result = {
        "method": "trackingClick",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerMoreInfo(trackingObj) {
    var result = {
        "method": "trackingMoreInfo",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyMore(trackingObj) {
    var result = {
        "method": "trackingQtyMore",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyLess(trackingObj) {
    var result = {
        "method": "trackingQtyLess",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerQtyChange(trackingObj) {
    var result = {
        "method": "trackingQtyChange",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerAddToList(trackingObj) {
    var result = {
        "method": "trackingAddToList",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}

function trackerBasket(basket, trackingObj) {
    var result = {
        "method": "trackbasket",
        "basket": JSON.stringify(basket),
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");
}


window.addEventListener("message", function(e) {
    var data;
    try {
        data = JSON.parse(e.data);
    } catch (e) {
        return;
    }

    switch (data.method) {

        case "crawl":
            jajoute(data.value);
            break;
        case "crawlwaters":
            watersProds(data.value);
            break;
        case "crawlpurina":
            purinaProds(data.value);
            break;

    }
});

post("crawlWater");

var watersProds = function(lecrawl) {

    var productos = settings['waters']['produits'];
    // console.log("CRAWL");
    // console.log(lecrawl);

    var len = $.map(lecrawl, function(n, i) {
        return i;
    }).length;

    $.each(productos, function(i, val) {

        var numProds = 0;
        var pordContainer = $('<div class="sto-hydratation-products-' + i + '"></div>');


        for (var a = 0; a < val.length; a++) {
            var index = val[a];

            if (lecrawl[index]) {
                var value = lecrawl[index];

                var cerealesFullProd = value['fullprod'];
                pordContainer.append(cerealesFullProd);
                numProds++;
            }


        }

        //$('.sto-hydratation-products-'+ i).attr('data-prod','3');

        $('.sto-hydratation-' + i + '-carrousel').append(pordContainer);
        $('.sto-hydratation-products-' + i).attr('data-prod', numProds);

        $('.sto-purina-frame1-decou-chiens').on('click', function(e) {
            post("scrolldog");
        });
        $('.sto-purina-frame1-decou-chats').on('click', function(e) {
            post("scrollcat");
        });
        $('.sto-hydratation-frame1-savoir').on('click', function(e) {
            post("scrollensavoir");
        });
        $('.sto-mbr-waterbrands-decou').on('click', function(e) {
            post("scrollwaterprod");
        });
    });

    var widthPaquets = $('.sto-hydratation-products-eau_plate').attr('data-prod') * 224;
    $('.sto-hydratation-products-eau_plate').css('width', widthPaquets + 'px');

    var widthBarres = $('.sto-hydratation-products-eau_gazeuse').attr('data-prod') * 224;
    $('.sto-hydratation-products-eau_gazeuse').css('width', widthBarres + 'px');

    var imagenes = $('#sto-mbr-tg-hydratation .item .picture img.photo, #sto-mbr-tg-hydratation .item .picture img:not(".photo"), #sto-mbr-tg-global .avantages p img, #sto-mbr-tg-hydratation img.ribbon');
    console.log(imagenes);
    for (var b = 0; b < imagenes.length; b++) {
        var srcImg = $(imagenes[b]).attr('src');
        if(srcImg[0] != "/"){
            $(imagenes[b]).attr('src', '//www.coradrive.fr/' + srcImg);
        }else{
            $(imagenes[b]).attr('src', '//www.coradrive.fr' + srcImg);
        }

    }

    $('#sto-mbr-tg-global .avantages p img').remove();


    // <editor-fold> CAROUSEL INDICATORS **************************************************
    $('.sto-hydratation-frame2-nav-item:nth-child(1)').on('click', function() {
        $('.sto-hydratation-products-eau_plate').animate({
            left: "0",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
        $('.sto-hydratation-frame2-nav-item:nth-child(1)').addClass('sto-hydratation-frame2-nav-current');
    });

    $('.sto-hydratation-frame2-nav-item:nth-child(2)').on('click', function() {
        $('.sto-hydratation-products-eau_plate').animate({
            left: "-896",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
        $('.sto-hydratation-frame2-nav-item:nth-child(2)').addClass('sto-hydratation-frame2-nav-current');
    });

    // $('.sto-hydratation-frame2-nav-item:nth-child(3)').on('click', function() {
    //     $('.sto-hydratation-products-eau_plate').animate({
    //         left: "-1792",
    //     }, 500, function() {
    //         // Animation complete.
    //     });
    //     $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
    //     $('.sto-hydratation-frame2-nav-item:nth-child(3)').addClass('sto-hydratation-frame2-nav-current');
    // });
    // </editor-fold> *********************************************************************


    // <editor-fold> CAROUSEL ARROWS ******************************************************
    $('.sto-hydratation-frame2-right').on('click', function() {

      switch ($('.sto-hydratation-products-eau_plate').css('left')) {
          case '0px':
                $('.sto-hydratation-products-eau_plate').animate({
                    left: "-896",
                }, 500, function() {
                    // Animation complete.
                });
                $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
                $('.sto-hydratation-frame2-nav-item:nth-child(2)').addClass('sto-hydratation-frame2-nav-current');
          break;

          // case '-896px':
          //       $('.sto-hydratation-products-eau_plate').animate({
          //           left: "-1792",
          //       }, 500, function() {
          //           // Animation complete.
          //       });
          //       $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
          //       $('.sto-hydratation-frame2-nav-item:nth-child(3)').addClass('sto-hydratation-frame2-nav-current');
          // break;
          default:
      }
    });

    $('.sto-hydratation-frame2-left').on('click', function() {

      switch ($('.sto-hydratation-products-eau_plate').css('left')) {
          case '-896px':
                $('.sto-hydratation-products-eau_plate').animate({
                    left: "0",
                }, 500, function() {
                    // Animation complete.
                });
                $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
                $('.sto-hydratation-frame2-nav-item:nth-child(1)').addClass('sto-hydratation-frame2-nav-current');
          break;

          // case '-1792px':
          //       $('.sto-hydratation-products-eau_plate').animate({
          //           left: "-896",
          //       }, 500, function() {
          //           // Animation complete.
          //       });
          //       $('.sto-hydratation-frame2-nav-item').attr('class', 'sto-hydratation-frame2-nav-item');
          //       $('.sto-hydratation-frame2-nav-item:nth-child(2)').addClass('sto-hydratation-frame2-nav-current');
          // break;
          default:
      }
    });
    // </editor-fold> *********************************************************************


    $('.sto-hydratation-frame3-left, .sto-hydratation-frame3-nav-item:nth-child(1)').on('click', function() {
        $('.sto-hydratation-products-eau_gazeuse').animate({
            left: "0",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-hydratation-frame3-nav-item').attr('class', 'sto-hydratation-frame3-nav-item');
        $('.sto-hydratation-frame3-nav-item:nth-child(1)').addClass('sto-hydratation-frame3-nav-current');
    });
    $('.sto-hydratation-frame3-right, .sto-hydratation-frame3-nav-item:nth-child(2)').on('click', function() {
        $('.sto-hydratation-products-eau_gazeuse').animate({
            left: "-896",
        }, 500, function() {
            // Animation complete.
        });
        $('.sto-hydratation-frame3-nav-item').attr('class', 'sto-hydratation-frame3-nav-item');
        $('.sto-hydratation-frame3-nav-item:nth-child(2)').addClass('sto-hydratation-frame3-nav-current');
    });


    $('#sto-mbr-tg-hydratation .item .actions .ajout-panier').on('click', function() {
        var product = $(this).attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerBasket([{
            "id": product,
            "name": name,
            "price": price,
            "qty": quantity,
            "promo": promo
        }], {
            "ta": "abk",
            "te": "abk-btn"
        });
        var prodBuyCereales = $(this).closest('.actions').addClass('sto-prod-buy').wrap('<div>').parent().html();
        post("addtocart", prodBuyCereales);
    });

    $('#sto-mbr-tg-hydratation .item .actions .liste').on('click', function() {

        var prodSaveCereales = $(this).closest('.actions').addClass('sto-prod-save').wrap('<div>').parent().html();
        post("addtolist", prodSaveCereales);
    });

    $('#sto-mbr-tg-hydratation .item .actions .plus').on('click', function() {
        var quantity = parseInt($(this).closest('.actions').find('input[name="qty"]').val());
        $(this).closest('.actions').find('input[name="qty"]').attr('value', quantity + 1);
    });
    $('#sto-mbr-tg-hydratation .item .actions .moins').on('click', function() {
        var quantity = parseInt($(this).closest('.actions').find('input[name="qty"]').val());
        if (quantity > 1) {
            $(this).closest('.actions').find('input[name="qty"]').attr('value', quantity - 1);
        }
    });
    $('input[name="qty"]').on('keyup', function() {
        var quantity = $(this).val();
        if (quantity < 1 || !quantity) {
            quantity = 1;
            $(this).val(quantity);
        }
        $(this).attr('value', quantity);
        $(this).blur();
    });

    var qteProdsPaquets = $('.sto-hydratation-products-eau_plate .item').length;
    console.log(qteProdsPaquets);
    var qteProdsBarres = $('.sto-hydratation-products-eau_gazeuse .item').length;
    if (qteProdsPaquets < 5) {
        $('.sto-hydratation-frame2-left, .sto-hydratation-frame2-right, .sto-hydratation-frame2-nav').hide();
    }
    if (qteProdsPaquets > 0) {
        $('.sto-hydratation-frame2-noprod').hide();
    }

    if (qteProdsBarres < 5) {
        $('.sto-hydratation-frame3-left, .sto-hydratation-frame3-right, .sto-hydratation-frame3-nav').hide();
    }
    if (qteProdsBarres > 0) {
        $('.sto-hydratation-frame3-noprod').hide();
    }

    $('#sto-mbr-tg-hydratation .actions .liste').on("click", function() {
        var producto = $(this).attr('data-prd');
        trackerAddToList({
            "tl": producto,
        });
    });
    $('#sto-mbr-tg-hydratation .actions .moins').on("click", function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyLess([{
          "id": product,
          "qty": quantity,
          "promo": promo,
          "name": name,
          "price": price
      }]);
    });
    $('#sto-mbr-tg-hydratation .actions .plus').on("click", function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyMore([{
          "id": product,
          "qty": quantity,
          "promo": promo,
          "name": name,
          "price": price
      }]);
    });
    $('#sto-mbr-tg-hydratation .actions input').on('change', function() {
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var product = $(this).closest('.actions').find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.item').find('div.desc p a').text();
        var quantity = $(this).closest('.item').find('input[name="qty"]').val();
        var price = $(this).closest('.item').find('.prix-produit').text();
        var promo;
        if ($(this).closest('.item').find('.avantages').length > 0) {
            promo = true;
        } else {
            promo = false;
        }
        trackerQtyMore([{
          "id": product,
          "qty": quantity,
          "promo": promo,
          "name": name,
          "price": price
      }]);
    });
    $('#sto-mbr-tg-hydratation .picture a, #sto-mbr-tg-purina .desc p a').on('click', function(e){
      e.preventDefault();
      var product = $(this).closest('.item').find('.ajout-panier').attr('data-prd');
      trackerMoreInfo({
          "tl": product,
      });
      var productLink = $(this).attr('href');
      post("seeprod", productLink);
    });


};



$('.sto-mbr-tg-jecontinue').on('click', function(e) {
    e.preventDefault();
    post("outofmbr");
});
$('.sto-mbr-tg-logo, .nav-recettes').on('click', function(e) {
    e.preventDefault();
    post("gotohome", "home");
});

$('.sto-mbr-nav-item.nav-equilibre').on('click', function(e) {
    e.preventDefault();
    post("gotohome", "equilibre");
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function(e) {
    e.preventDefault();
    post("gotocereales");
});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function(e) {
    e.preventDefault();
    post("gotopurina");
});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function(e) {

});

setInterval(function(){
    if($(document).find('.avantages').length != 0){
        $(document).find('.avantages').on('click', function(e) {
            if($(this).find("ul").hasClass("open")){
                $(this).find("ul").removeClass("open");
            }else{
                $(this).find("ul").addClass("open");
            }
        });
    }
},20);


//$(".sto-recette-icons").mCustomScrollbar();


/****************************      TRACKIIIIIIIIIIING      *************************************/

/*****  header  *****/
$('.sto-mbr-tg-jecontinue').on('click', function() {
    trackerClick({
        "tl": "Close"
    });
});
$('.sto-mbr-tg-logo').on('click', function() {
    trackerClick({
        "tl": "HomeLogo"
    });
});
$('.sto-mbr-nav-item.nav-recettes').on('click', function() {
    trackerClick({
        "tl": "MainNavHome"
    });
});
$('.sto-mbr-nav-item.nav-equilibre').on('click', function() {
    trackerClick({
        "tl": "MainNavEquilibre"
    });
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function() {
    trackerClick({
        "tl": "MainNavCereales"
    });
});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function() {
    trackerClick({
        "tl": "MainNavPurina"
    });
});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function(e) {
    trackerClick({
        "tl": "MainNavHydratation"
    });
});

/*****  Hydratation  *****/
$('.sto-hydratation-frame1-decou-eau_plate').on("click", function() {
    trackerClick({
        "tl": "DecouvirPaquets"
    });
});
$('.sto-hydratation-frame1-decou-eau_gazeuse').on("click", function() {
    trackerClick({
        "tl": "DecouvirBarres"
    });
});
$('.sto-hydratation-frame1-savoir').on("click", function() {
    trackerClick({
        "tl": "hydratationEnSavoirPlus"
    });
});
$('.sto-mbr-waterbrands-decou').on('click', function(e) {
    trackerClick({
        "tl": "JeDecouvreLesProduits"
    });
});
$('.sto-hydratation-frame2-left').on("click", function() {
    trackerBrowse({
        "tl": "NavEauPlateLeft"
    });
});
$('.sto-hydratation-frame2-right').on("click", function() {
    trackerBrowse({
        "tl": "NavEauPlateLeft"
    });
});
$('.sto-hydratation-frame2-nav').on("click", function() {
    trackerBrowse({
        "tl": "NavEauPlate"
    });
});

$('.sto-hydratation-frame3-left').on("click", function() {
    trackerBrowse({
        "tl": "NavEauGazeuseLeft"
    });
});
$('.sto-hydratation-frame3-right').on("click", function() {
    trackerBrowse({
        "tl": "NavEauGazeuseLeft"
    });
});
$('.sto-hydratation-frame3-nav').on("click", function() {
    trackerBrowse({
        "tl": "NavEauGazeuse"
    });
});
