"use strict";
//var Vue = require("vue");
var $ = require("jquery");
var style = require("./style.css");
//var corastyle = require("./coradrive.css");
var template_hydratation = require("./template_hydratation.html");
var recettes = require("../recettes.json");
var sto = window.__sto;

style.use();

function post(method, value) {
    var result = {
        "method": method
    };
    if (value !== undefined) {
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");
}


$("#sto-mbr-tg-content").append(template_hydratation);
