"use strict";
__sto.load("TG", function(tracker) {
    // require("es5-shim/es5-shim");
    // require('es5-shim/es5-sham');
    tracker.display();
    tracker.view();
    const document = global.document;
    const $ = global.jQuery;
    // const baseUrl = "//localhost:8080";
    const baseUrl = "//rscdn.storetail.net/coradrive/nestle/mesbonsreflexesV3/homepage/";
    const iframe = document.createElement("IFRAME");
    var settings = require("./setting.json");
    var results = {};

    $('body').css('background-image', 'none');

    iframe.src = baseUrl;
    iframe.id = 'mbrapp';

    $('.wrapinner h1').text('Recettes et équilibre alimentaire');

    var drive = document.location.toString().split('/')[3];
    if (document.location.toString().split('?').length > 1) {
        var recettesurl = document.location.toString().split('?')[1];
    }


    function post(method, value) {
        var result = {
            "method": method
        };
        if (value !== undefined) {
            result.value = value;
        }
        var win = document.getElementById("mbrapp").contentWindow;
        win.postMessage(JSON.stringify(result), "*");

    }

    function postHome() {

    }






    function trackerBrowse(data) {
        tracker.browse(JSON.parse(data.value));
    }
    function trackerClick(data) {
        tracker.click(JSON.parse(data.value));
    }

    function trackbasket(data) {
        tracker.basket(JSON.parse(data.basket), JSON.parse(data.value));
    }

    function trackerMethod(trackingObj) {
        var result = {
            "method": "tracking",
            "value": JSON.stringify(trackingObj)
        };
        window.postMessage(JSON.stringify(result), "*");
    }


    window.addEventListener("message", function(e) {
        var data;
        try {
            data = JSON.parse(e.data);
        } catch (e) {
            return;
        }
        switch (data.method) {

            case "trackingBrowse":
                trackerBrowse(data);
                break;
            case "trackingClick":
                trackerClick(data);
                break;
            case "trackbasket":
                trackbasket(data);
                break;
            case "selectcrawl":
                crawler(data.value);
                //post("crawl", results);
                break;
            case "addtocart":
                addtocart(data.value);
                break;
            case "addtolist":
                addtolist(data.value);
                break;
            case "scrolltop":
                scrolltop();
                break;
            case "scrolldog":
                scrolldog();
                break;
            case "scrollcat":
                scrollcat();
                break;
            case "scrollensavoir":
                scrollensavoir();
                break;
            case "bigtg":
                bigtg();
                break;
            case "smalltg":
                smalltg();
                break;
            case "smallertg":
                smallertg();
                break;
            case "outofmbr":
                outofmbr();
                break;
            case "gotopurina":
                gotopurina();
                break;
            case "gotocereales":
                gotocereales();
                break;
            case "gotowater":
                gotowater();
                break;
            case "askhomeurl":
                gethomeurl();
                break;
            case "seeprod":
                seeprod(data.value);
                break;
        }
    });





    function crawler(k) {

        var crawlLinkOriginal = settings[k]['crawl_' + k].toString().split('/');
        console.log(crawlLinkOriginal);
        crawlLinkOriginal[3] = drive;
        var crawlLinkNew = '';

        for (var d = 0; d < crawlLinkOriginal.length; d++) {
            crawlLinkNew = crawlLinkNew + crawlLinkOriginal[d] + '/';
            console.log(crawlLinkNew);
        }

        $.get(crawlLinkNew, function(data) {

            $.when($(data).find(".item").each(function(i, v) {

                if (!$(this).hasClass('soon_available')) {
                  var p = {
                      "id": $(v).find(".idProduit").attr("value"),
                      "libelle": $(v).find(".desc p a").text(),
                      "img": $(v).find(".picture a img").attr('src').substring(1),
                      "prix": $(v).find(".price .prix-produit").html(),
                      "prixsmall": $(v).find(".price .info").html().split('<br>'),
                      "link": $(v).find(".picture a").attr('href'),
                      "addtocart": $(v).find(".actions").wrap('<div>').parent().html()
                  };

                  results[p.id] = p;
                }



            })).done(post("crawl", results));

        });
        //setTimeout(function() {
        //    post("crawl", results);
        //}, 1000);
        return results;
    }

    function seeprod(data) {
        window.open(data, "_self");
    }

    function gethomeurl() {
        post('sendhomeurl', recettesurl)
    }

    function scrolltop() {
        $('html, body').animate({
            scrollTop: 0
        }, 0);
    }

    function scrolldog() {
        $('html, body').animate({
            scrollTop: 1025
        }, 1000);
    }

    function scrollcat() {
        $('html, body').animate({
            scrollTop: 456
        }, 500);
    }

    function scrollensavoir() {
        $('html, body').animate({
            scrollTop: 1603
        }, 1000);
    }

    function bigtg() {
        $('body').find('#main').css("height", "2150px");
        $('body').find('#main>div').css("height", "2181px");
    }

    function smalltg() {
        $('body').find('#main').css("height", "830px");
        $('body').find('#main>div').css("height", "861px");
    }

    function smallertg() {
        $('body').find('#main').css("height", "1700px");
        $('body').find('#main>div').css("height", "1731px");
    }

    function outofmbr() {
        window.open("http://www.coradrive.fr/"+drive+"/", "_self");
        //location.reload();
    }

    function gotopurina() {
        console.log("voy a purina");
        window.open("http://www.coradrive.fr/"+drive+"/pages/evenement3.html", "_self");
    }

    function gotocereales() {
        console.log("voy a cereales");
        window.open("http://www.coradrive.fr/"+drive+"/pages/evenement2.html", "_self");
    }

    function gotowater() {
        console.log("voy a water");
        window.open("http://www.coradrive.fr/"+drive+"/pages/evenement4.html", "_self");
    }

    function addtocart(data) {
        $('body').append(data);
        $('.sto-prod-buy').hide();
        $('.sto-prod-buy .ajout-panier').last().addClass('ajout-panier-externe').removeClass('ajout-panier');
        $('.sto-prod-buy .ajout-panier-externe').last().click();
    }

    function addtolist(data) {
        $('body').append(data);
        $('.sto-prod-save').hide();
        $('.sto-prod-save .liste').last().click();
    }

    $('html').removeClass("headerpasfixe");
    $('#lockheader').hide();



    $(function() {

        const $content = $("#main").empty();
        $(".wrapinnertop").css("position","relative");
        $("body").css("background-image", "none");

        $content.css("height", "861px");
        const $container = $("<div>")
            .css({
                height: "861px",
                position: "absolute",
                left: "0",
                right: "0",
                marginTop: "-1px"
            })
            .append(
                $(iframe)
                .css({
                    position: "relative",
                    width: "100%",
                    height: "100%"
                })
            )
            .appendTo("#main");
    });

    $('html, body').animate({
        scrollTop: 0
    }, 0);

});
