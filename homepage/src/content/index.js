"use strict";
// var Vue = require("vue");
var $ = require("jquery");
var style = require("./style.css");
var template_home = require("./template_home.html");
var template_recette_etapes = require("./template_recette_etapes.html");
var template_equilibre = require("./template_equilibre.html");
var recettes = require("../recettes.json");
var sto = window.__sto;

//var ifequilibre = window.parent.document.location.toString();

//console.log(ifequilibre);

style.use();

function post(method, value) {
    var result = {
        "method": method
    };
    if (value !== undefined) {
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");

}

window.addEventListener("message", function(e) {
    var data;
    try {
        data = JSON.parse(e.data);
    } catch (e) {
        return;
    }

    switch (data.method) {

        case "sendhomeurl":
            publishhome(data.value);
            break;

    }
});

var publishhome = function(data){
    if(data == "equilibre"){
      $("#sto-mbr-tg-equilibre").show();
      $("#sto-mbr-tg-ficherecette-etapes").hide();
      $("#sto-mbr-tg-recettes").hide();
      $(".sto-mbr-nav-item").removeAttr('id');
      $(".sto-mbr-nav-item.nav-equilibre").attr('id', 'sto-mbr-nav-current');
    }
}

$("#sto-mbr-tg-content").append(template_home);
$("#sto-mbr-tg-content").append(template_recette_etapes);
$("#sto-mbr-tg-content").append(template_equilibre);
$("#sto-mbr-tg-equilibre").hide();
$("#sto-mbr-tg-ficherecette-etapes").hide();

var select_recette = function() {
    $('.sto-recettes-list-item').on('click', function() {
        var choice = $(this).attr('data-name');
        var recetteInfos = recettes[choice];

        $("#sto-mbr-tg-recettes").hide();
        $("#sto-mbr-tg-ficherecette-etapes").show();
        $('.sto-fiche-recette-visuel').attr('class', 'sto-fiche-recette-visuel');
        $('.sto-fiche-recette-visuel').addClass('visuel_' + choice);
        $('.sto-fiche-recette-title').attr('class', 'sto-fiche-recette-title');
        $('.sto-fiche-recette-title').addClass('recette-title_' + choice);

        if (recetteInfos['type'] == 'etapes') {
            $('.sto-recette-content-title').text('La recette par étapes');

            for (var i = 0; i < recetteInfos['etapes'].length; i++) {
                $('<p class="sto-etape-subtitle">Etape ' + (i + 1) + '</p>').appendTo('.sto-recette-content-etapes');
                $('<p class="sto-etape-text">' + recetteInfos['etapes'][i] + '</p>').appendTo('.sto-recette-content-etapes');
            }
            $('.recette-footer-imprimer-link').attr('href', recetteInfos['print']);
            $('.recette-footer-envoyer-link').attr('href', 'mailto:?subject=Recette&body=Bonjour, %0A %0A Voici le lien de téléchargement de la recette \"' + recetteInfos['name'] + '\" %0A %0A ' + recetteInfos['print'] + ' %0A %0A Bon appétit !');

            var specsRecette = $('<p class="sto-recette-visual-spec-01">' + recetteInfos['spec'][0] + '</p><p class="sto-recette-visual-spec-02">' + recetteInfos['spec'][1] + '</p><p class="sto-recette-visual-spec-03">' + recetteInfos['spec'][2] + '</p>');
            $('.sto-fiche-recette-visuel-specs').append(specsRecette);

        } else {
            $('.sto-recette-content-title').text('La recette en vidéo');
            $('<div class="sto-recette-video"><iframe class="recette-video-iframe" id="recette-video-' + choice + '" src="' + recetteInfos['video'] + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>').appendTo('.sto-recette-content-etapes');
            $('<div class="sto-recette-icons"></div>').appendTo('.sto-recette-content-etapes');
            $('.recette-footer-imprimer-link').attr('href', recetteInfos['print']);
            $('.recette-footer-envoyer-link').attr('href', 'mailto:?subject=Recette&body=Bonjour, %0A %0A Voici le lien de téléchargement de la recette \"' + recetteInfos['name'] + '\" %0A %0A ' + recetteInfos['print'] + ' %0A %0A Bon appétit !');
            var specsRecette = $('<p class="sto-recette-visual-spec-01">' + recetteInfos['spec'][0] + '</p><p class="sto-recette-visual-spec-02">' + recetteInfos['spec'][1] + '</p><p class="sto-recette-visual-spec-03">' + recetteInfos['spec'][2] + '</p>');
            $('.sto-fiche-recette-visuel-specs').append(specsRecette);
        }


        var cosa = $(recetteInfos['ingredients']);
        var quant = '';
        for (var i = 0; i < recetteInfos['ingredients'].length; i++) {
            $('<li class="sto-recette-ingredients-item">' + recetteInfos['ingredients'][i][0] + '<span class="sto-recette-ingredients-quant"> ' + recetteInfos['ingredients'][i][1] + '</span></li>').appendTo('.sto-recette-ingredients-list');

        }

        console.log(choice);

        // suprrimer liste ingrédients pizza
        if (choice == "buitoni") {
          $('.sto-recette-ingredients').hide();
          $('.sto-jajoute-title').text('Je choisis ma pizza');
          $('.sto-recette-footer').css({
            'margin-top': '58px'
          });
        } else {
          $('.sto-recette-ingredients').show();
          $('.sto-jajoute-title').text('J\'ajoute mes ingrédients au panier');
          $('.sto-recette-footer').css({
            'margin-top': '10px'
          });
        }

        // asterisque Maggi
        if (choice == "maggi") {
          $('.sto-recette-content-etapes .sto-etape-subtitle').css({
            'margin-top': "10px"
          });
          $('.sto-recette-ingredients').css({
            'margin-top': "10px"
          });
          $('.sto-recette-content-etapes .sto-etape-text').css({
            'font-style': 'normal',
            'font-size': "13px",
            'margin-top': "7px"
          });
          $('.sto-recette-content-etapes .sto-etape-subtitle').eq(4).hide();
          $('.sto-recette-content-etapes .sto-etape-text').eq(4).css({
            'font-style': 'normal',
            'font-size': "10px",
            'margin-top': "5px"
          });
          $('.sto-recette-ingredients-item:last-of-type').css({
            'width': 'auto'
          });
        }


        post("scrolltop");



    });


};
select_recette();

var equilibre_carrousel = function() {

    $('.sto-astuces-right').on('click', function() {
        for (var i = 1; i <= $('.sto-equilibre-astuces-carrousel-item').length; i++) {
            var num = parseInt($('.sto-equilibre-astuces-carrousel-item[data-item="' + i + '"]').attr('id')) - 1;
            if (num == 0) {
                num = 3
            }
            $('.sto-equilibre-astuces-carrousel-item[data-item="' + i + '"]').attr('id', num);
        }
        $('#1.sto-equilibre-astuces-carrousel-item').animate({
            left: "0",
        }, 300, function() {
            // Animation complete.
        });
        $('#2.sto-equilibre-astuces-carrousel-item').animate({
            left: "240",
        }, 0, function() {
            // Animation complete.
        });
        $('#3.sto-equilibre-astuces-carrousel-item').animate({
            left: "-240",
        }, 300, function() {
            // Animation complete.
        });

        var navSpot = $('#1.sto-equilibre-astuces-carrousel-item').attr('data-item');
        $('.sto-astuce-dot').attr('class', 'sto-astuce-dot');
        $('.sto-astuce-dot[data-item="' + navSpot + '"]').addClass('sto-astuce-dot-current');

    });

    $('.sto-astuces-left').on('click', function() {

        for (var i = 1; i <= $('.sto-equilibre-astuces-carrousel-item').length; i++) {
            var num = parseInt($('.sto-equilibre-astuces-carrousel-item[data-item="' + i + '"]').attr('id')) + 1;
            if (num == 4) {
                num = 1
            }
            $('.sto-equilibre-astuces-carrousel-item[data-item="' + i + '"]').attr('id', num);
        }
        $('#1.sto-equilibre-astuces-carrousel-item').animate({
            left: "0",
        }, 300, function() {
            // Animation complete.
        });
        $('#2.sto-equilibre-astuces-carrousel-item').animate({
            left: "240",
        }, 300, function() {
            // Animation complete.
        });
        $('#3.sto-equilibre-astuces-carrousel-item').animate({
            left: "-240",
        }, 0, function() {
            // Animation complete.
        });

        var navSpot = $('#1.sto-equilibre-astuces-carrousel-item').attr('data-item');
        $('.sto-astuce-dot').attr('class', 'sto-astuce-dot');
        $('.sto-astuce-dot[data-item="' + navSpot + '"]').addClass('sto-astuce-dot-current');

    });
    $('.sto-astuce-dot').on('click', function() {
        var oldNav = parseInt($('.sto-astuce-dot.sto-astuce-dot-current').attr('data-item'));
        var newNav = parseInt($(this).attr('data-item'));

        if (newNav == (oldNav + 1)) {
            $('.sto-astuces-right').click();
        } else if (newNav == (oldNav + 2)) {
            $('.sto-astuces-right').click();
            setTimeout(function() {
                $('.sto-astuces-right').click();
            }, 300);
        } else if (newNav == (oldNav - 1)) {
            $('.sto-astuces-left').click();
        } else if (newNav == (oldNav - 2)) {
            $('.sto-astuces-left').click();
            setTimeout(function() {
                $('.sto-astuces-left').click();
            }, 300);
        }

        $('.sto-astuce-dot').attr('class', 'sto-astuce-dot');
        $(this).addClass('sto-astuce-dot-current');

    });


};
equilibre_carrousel();
