"use strict";
var $ = require("jquery");
var header = require("./src/header/index.js");
var recettes_content = require("./src/content/index.js");
var settings = require("./setting.json");
var template_home = require("./src/content/template_home.html");
var template_equilibre = require("./src/content/template_equilibre.html");
var sto = window.__sto;
var current;


function post(method, value) {
    var result = {
        "method": method
    };
    if (value !== undefined) {
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");

}

function trackerBrowse(trackingObj) {
    var result = {
        "method": "trackingBrowse",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");

}

function trackerClick(trackingObj) {
    var result = {
        "method": "trackingClick",
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");

}

function trackerBasket(basket, trackingObj) {
    var result = {
        "method": "trackbasket",
        "basket": JSON.stringify(basket),
        "value": JSON.stringify(trackingObj)
    };
    window.parent.postMessage(JSON.stringify(result), "*");

}

$('.sto-recettes-list-item').on('click', function() {
    post("selectcrawl", $(this).attr('data-name'))
    current = $(this).attr('data-name');
    //post("crawl", results);
});


window.addEventListener("message", function(e) {
    var data;
    try {
        data = JSON.parse(e.data);
    } catch (e) {
        return;
    }

    switch (data.method) {

        case "crawl":
            jajoute(data.value);
            break;
        case "crawlcereales":
            cerealesProds(data.value);
            break;
        case "crawlpurina":
            purinaProds(data.value);
            break;

    }
});

var askHomeUrl = function() {
    post("askhomeurl");
}
askHomeUrl();

var jajoute = function(lecrawl) {


    var productos = settings[current]['produits'];


    var len = $.map(lecrawl, function(n, i) {
        return i;
    }).length;

    $.each(productos, function(i, val) {

        var icons = $('<div class="video-prod-ico" data-ico="' + val[1] + '"  title="' + i + '"></div>');
        $('.sto-recette-icons').append(icons);
        var onglet = $('<li class="sto-jajoute-store-list-item" data-ico="' + val[1] + '"><p>' + i + '</p></li>');
        var ongletList = $('<ul class="sto-jajoute-prods-list"></ul>');
        $('.sto-jajoute-store-list').append(onglet);
        onglet.append(ongletList);

        for (var a = 0; a < val[0].length; a++) {
            var index = val[0][a];

            if (lecrawl[index]) {
                var value = lecrawl[index];

                var jajouteProdCont = $('<li>').addClass('sto-jajoute-prods-list-item clearfix');
                var jajouteProdLab = $('<div class="sto-jajoute-prod-label"><a href="' + value['link'] + '">' + value['libelle'] + '</a></div>');
                var jajouteProdImg = $('<a href="' + value['link'] + '"><img class="sto-jajoute-prod-img" src="http://www.coradrive.fr/' + value['img'] + '" alt="' + value['libelle'] + '"/></a>');
                var jajouteProdPrixBig = $('<div class="sto-jajoute-prod-prix">' + value['prix'] + '</div>');
                var jajouteProdPrixSmall = $('<div class="sto-jajoute-prod-prixsmall">' + value['prixsmall'][0] + ' ' + value['prixsmall'][1] + '</div>');
                var jajouteProdCta = $('<div class="sto-jajoute-prod-cta" data-id="' + value['id'] + '"></div>');
                var jajouteAdd = value['addtocart'];

                jajouteProdCta.append(jajouteAdd);
                jajouteProdCont.append(jajouteProdLab, jajouteProdImg, jajouteProdPrixBig, jajouteProdPrixSmall, jajouteProdCta);
                ongletList.append(jajouteProdCont);
            }

        }

    });

    $('.sto-jajoute-store-list-item p').on('click', function() {

        $('.sto-jajoute-prods-list').slideUp();
        if ($(this).closest('.sto-jajoute-store-list-item p').hasClass("sto-ingredients-open")) {
            $('.sto-jajoute-store-list-item p').removeClass("sto-ingredients-open");
        } else {
            $('.sto-jajoute-store-list-item p').removeClass("sto-ingredients-open");
            $(this).closest('.sto-jajoute-store-list-item p').addClass("sto-ingredients-open");
            $(this).closest('.sto-jajoute-store-list-item').find('.sto-jajoute-prods-list').slideToggle("fast", function() {});
        }
    });

    var noProd = $('<div class="sto-mess-no-por">Produit temporairement indisponible</div>');
    $('.sto-jajoute-prods-list:empty').append(noProd);

    $('.sto-jajoute-prod-cta').on('click', function() {
        var product = $(this).find('.ajout-panier').attr('data-prd');
        var name = $(this).closest('.sto-jajoute-prods-list-item').find('div.sto-jajoute-prod-label a').text();
        var quantity = 1;
        var price = $(this).closest('.sto-jajoute-prods-list-item').find('.sto-jajoute-prod-prix').text();
        var promo = false;

        trackerBasket([{
            "id": product,
            "name": name,
            "price": price,
            "qty": quantity,
            "promo": promo
        }], {
            "ta": "abk",
            "te": "abk-btn"
        });
        var prodBuy = $(this).find('.actions').addClass('sto-prod-buy').wrap('<div>').parent().html();
        post("addtocart", prodBuy);
    });
    $('.video-prod-ico').on('click', function() {

        $('.sto-jajoute-prods-list').slideUp();
        if ($('.sto-jajoute-store-list-item[data-ico="' + $(this).attr('data-ico') + '"] p').hasClass("sto-ingredients-open")) {
            $('.sto-jajoute-store-list-item p').removeClass('sto-ingredients-open');
        } else {
            $('.sto-jajoute-store-list-item p').removeClass('sto-ingredients-open');
            $('.sto-jajoute-store-list-item[data-ico="' + $(this).attr('data-ico') + '"]').find('.sto-jajoute-prods-list').slideToggle("fast", function() {});
            $('.sto-jajoute-store-list-item[data-ico="' + $(this).attr('data-ico') + '"] p').toggleClass("sto-ingredients-open");
        }

    });

    $('.sto-jajoute-store-list-item p').on('click', function() {
        trackerBrowse({
            "tl": "NavListIngredient"
        });
    });
    // $('.video-prod-ico').on('click', function() {
    //     trackerMethod({
    //         "tl": "NavIcoIngredient"
    //     });
    // });
    $('#sto-mbr-tg-ficherecette-etapes .sto-jajoute-prods-list-item>a, #sto-mbr-tg-ficherecette-etapes .sto-jajoute-prods-list-item .sto-jajoute-prod-label>a').on('click', function(e){
      e.preventDefault();
      var product = $(this).closest('.item').find('.ajout-panier').attr('data-prd');
      trackerClick({
          "tl": product,
      });
      var productLink = $(this).attr('href');
      post("seeprod", productLink);
    });



};


$('.sto-mbr-tg-logo, .nav-recettes, .recette-footer-voir').on('click', function() {
    $("#sto-mbr-tg-ficherecette-etapes").hide();
    $('.sto-jajoute-store-list, .sto-recette-content-etapes, .sto-recette-ingredients-list, .sto-fiche-recette-visuel-specs').empty();
    $("#sto-mbr-tg-equilibre").hide();
    $("#sto-mbr-tg-recettes").show();
    post("smalltg");
    post("scrolltop");
    $(".sto-mbr-nav-item").removeAttr('id');
    $(".nav-recettes").attr('id', 'sto-mbr-nav-current');
});
$('.sto-mbr-tg-jecontinue').on('click', function(e) {
    e.preventDefault();
    post("outofmbr");
});

$('.sto-mbr-nav-item.nav-equilibre').on('click', function() {
    $("#sto-mbr-tg-ficherecette-etapes").hide();
    $('.sto-jajoute-store-list, .sto-recette-content-etapes, .sto-recette-ingredients-list').empty();
    $("#sto-mbr-tg-recettes").hide();
    $("#sto-mbr-tg-equilibre").show();
    post("smalltg");
    post("scrolltop");
    $(".sto-mbr-nav-item").removeAttr('id');
    $(this).attr('id', 'sto-mbr-nav-current');
});

$('.sto-mbr-nav-item.nav-petitdej').on('click', function(e) {
    e.preventDefault();
    post("gotocereales");
    $(".sto-mbr-nav-item").removeAttr('id');
});

$('.sto-mbr-nav-item.nav-amisbetes').on('click', function(e) {
    e.preventDefault();
    post("gotopurina");
    $(".sto-mbr-nav-item").removeAttr('id');
});

$('.sto-mbr-nav-item.nav-hydratation').on('click', function(e) {
    e.preventDefault();
    post("gotowater");
    $(".sto-mbr-nav-item").removeAttr('id');
});

//$(".sto-recette-icons").mCustomScrollbar();


/**      TRACKING      **/

/*****  header  *****/
$('.sto-mbr-tg-jecontinue').on('click', function() {
    trackerClick({
        "tl": "Close"
    });
});
$('.sto-mbr-tg-logo').on('click', function() {
    trackerClick({
        "tl": "HomeLogo"
    });
});
$('.sto-mbr-nav-item.nav-recettes').on('click', function() {
    trackerClick({
        "tl": "MainNavHome"
    });
});
$('.sto-mbr-nav-item.nav-equilibre').on('click', function() {
    trackerClick({
        "tl": "MainNavEquilibre"
    });
});
$('.sto-mbr-nav-item.nav-petitdej').on('click', function() {
    trackerClick({
        "tl": "MainNavCereales"
    });
});
$('.sto-mbr-nav-item.nav-amisbetes').on('click', function() {
    trackerClick({
        "tl": "MainNavPurina"
    });
});
$('.sto-mbr-nav-item.nav-hydratation').on('click', function() {
    trackerClick({
        "tl": "MainNavHydratation"
    });
});

/*****  Home  *****/
$('.sto-recettes-list-item').on('click', function() {
    var trackName = $(this).attr('data-name');
    trackerClick({
        "tl": trackName
    });
});

/*****  Fiche recette  *****/

$('.recette-footer-voir').on('click', function() {
    trackerClick({
        "tl": "RecetteFooterHome"
    });
});
$('.recette-footer-imprimer').on('click', function() {
    trackerClick({
        "tl": "RecetteFooterPrint"
    });
});
$('.recette-footer-envoyer').on('click', function() {
    trackerClick({
        "tl": "RecetteFooterEmail"
    });
});
$('.recette-video-iframe').on('click', function() {
    trackerClick({
        "tl": "PlayVideo"
    });
});



/*****  Equilibre  *****/
$('.sto-astuces-right').on('click', function() {
    trackerClick({
        "tl": "AstucesNavRight"
    });
});
$('.sto-astuces-left').on('click', function() {
    trackerClick({
        "tl": "AstucesNavLeft"
    });
});
$('.sto-astuces-nav li').on('click', function() {
    trackerClick({
        "tl": "AstucesNavDown"
    });
});
// $('.sto-equilibre-pyramid-1').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverPyramid1"
//     });
// });
// $('.sto-equilibre-pyramid-2').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverPyramid2"
//     });
// });
// $('.sto-equilibre-pyramid-3').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverPyramid3"
//     });
// });
// $('.sto-equilibre-pyramid-4').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverPyramid4"
//     });
// });
// $('.sto-equilibre-pyramid-5').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverPyramid5"
//     });
// });
// $('.sto-equilibre-pyramid-6').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverOn6"
//     });
// });
// $('.sto-equilibre-pyramid-7').on("mouseenter mouseleave", function() {
//     trackerMethod({
//         "tl": "HoverPyramid7"
//     });
// });

$(".sto-jajoute-store-list-item").click(function(){
   $(".sto-jajoute-store-list-item").scrollTop();
   console.log($(this));
});
